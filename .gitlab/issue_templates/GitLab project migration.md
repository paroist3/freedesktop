<!--
     This form lets you request migration of your project from freedesktop.org's
     prior hosting of Git/Bugzilla/etc to GitLab.

     We would like to get a complete picture of what your project currently uses
     from us, as well as anything you would want us to do for you as well.
     Please take the below as a guide and fill in all relevant information it
     requests, but if you would like anything else, if you have any questions
     about any of the services, or just want to note something, please include
     it as freeform text.

     As you fill the sections, please check the boxes at the top; please also
     pay close attention to the contact and CoC sections and check those as
     appropriate. Once we get the migration underway, we'll start going through
     the items one by one and checking them off, so we have a good and clear
     view of the migration process.

     Thanks!
-->

## Checklist

- [ ] Project contacts filled by by requestor
- [ ] CoC section filled by requestor
- [ ] Git repo information filled by requestor (or N/A)
- [ ] Bugzilla information filled by requestor (or N/A)
- [ ] Mailman information filled by requestor (or N/A)
- [ ] Web hosting information filled by requestor (or N/A)
- [ ] Migration request acked & scheduled by admin
- [ ] Migration in progress

## Project contacts
<!--
     This is required for all projects; we will use it this if we need a
     decision representing the project as a whole. For instance, if someone is
     requesting access to your project, if copyright/license issues arise, or if
     we need to discuss something which affects all our member projects (e.g.
     the move to GitLab, or the introduction of the Code of Conduct), we will
     get in touch with those listed here and assume you are able to speak on
     behalf of the project.

     We will also make the primary admin (and additional contacts, if you
     request) the owner of the GitLab group. With this, you will be able to
     completely control access to your repos, and to create new repos, without
     fd.o admin intervention.

     Please list multiple contacts! If you need to amend the contacts, just
     reopen this issue and we'll be happy to help. If you prefer to be contacted
     on an email other than what's in your GitLab profile, please list that as
     well.
-->

**Primary project admin** (GitLab username):

**Additional project contacts** (GitLab usernames):


## Code of Conduct
<!--
     This is required for all projects.

     freedesktop.org maintains a unified Code of Conduct, which can be found at
     https://www.freedesktop.org/wiki/CodeOfConduct

     Our CoC extends across all our platforms - GitLab itself, mailing lists,
     hosted sites, and so on. In short, the CoC requires everyone participating
     in discussion on freedesktop.org to engage in good faith, to treat those
     around them like human beings, and to refrain from bullying behaviour,
     harrassment or discrimination.

     We prefer this to be the standard at project level: that contributors can
     walk into any project and expect that they will be treated with respect.
     Therefore, we recommend projects place a link to the CoC in places like
     README or CONTRIBUTING files (as well as a small mention alongside, e.g.,
     links to mailing lists or GitLab). This should contain a list of
     contacts for your project, who contributors who can raise any issues with.

     Whilst fd.o provides a last-level point of escalation for CoC issues,
     such as when people are uncomfortable raising issues with the project
     team directly, we prefer this to be a part of every project's day-to-day
     communications and ethos, rather than enforced on you from above.

     That being said, we accept that this was introduced without discussion
     with member projects, and you may have concerns or issues you want to
     discuss with us. You are welcome to either discuss those in this issue,
     or if you'd prefer to discuss with us privately, our personal email
     addresses are on the wiki page above.
-->

- [ ] My project agrees to the Code of Conduct
- [ ] Where reasonable, we have placed links to the CoC within our project

**CoC contacts** (if different from overall project contacts):


## Git repository migration
<!--
     Delete this section if you have no hosted Git repos.

     As part of the migration, we will transfer your primary repository
     access to GitLab. GitLab will serve as the sole point for you to push
     changes to, and we will mirror your repositories to cgit.fd.o and
     anongit.fd.o, which will remain as read-only mirrors. Trying to push to
     the mirrors will fail with an error message describing how to change the
     remote to use GitLab. The GitHub mirror at
     https://github.com/freedesktop-mirror/ will also be updated.

     If you have mirrors to other sites you would like to update, please
     describe them here and we will figure out a way to make it happen.

     Note that, for security reasons, this is separate from GitLab's
     push-mirror configuration you can see in the web UI, and will not be
     visible there.

     Though we recommend keeping paths constant for ease of use (e.g.
     cgit.fd.o/mesa/mesa -> gitlab.fd.o/mesa/mesa), we are able to rename them
     to different paths on GitLab if you would like. Please specify those here.

     Please also specify if you want your repositories configured a particular
     way: if you want issue tracking or merge requests disabled, etc.

     If you have had your repository hosted externally (e.g. GitHub) and would
     likely to migrate back, we can likely still migrate this for you. Please
     let us know if this is the case and we will see what we can do.
-->

- [ ] Migrate: `cgit.fd.o/repo/path` -> `gitlab.fd.o/repo/path`
- [ ] Migrate: `cgit.fd.o/project/foo` -> `gitlab.fd.o/project/bar`

## Bugzilla
<!--
     Delete this if you are not currently using Bugzilla, or don't want to
     migrate from an existing issue tracker.

     We are using a fork of GNOME's 'bztogl' script to migrate issues from
     Bugzilla to GitLab. For every bug currently open on Bugzilla, this will
     create a new GitLab issue, preserving the title and description, as well
     as comments and attachments. It will also attempt to associate Bugzilla
     users to GitLab users, and preserve bug assignments.

     If migrating an entire Bugzilla product (say 'ModemManager') to one GitLab
     project ('mobile-broadband/ModemManager'), this will add a label to
     GitLab with the component name. If migrating individual components, only
     the 'bugzilla' label will be added, which can be used to identify newly
     imported issues in need of triage or cleanup.

     On migration, your Bugzilla product will be closed for new issue entry;
     we are working to set up a redirect from the old Bugzilla bug-entry URL.
     Issues which are already closed on Bugzilla will _not_ be migrated; they
     will be preserved in amber for all time in the Bugzilla interface, with
     their old URLs.

     We can also migrate bugs from GNOME's Bugzilla or external Phabricator
     instances; again, if you are migrating from other external services, please
     let us know and we will see if we can help migmrate those.
-->

- [ ] Migrate bugs: all from Bugzilla product `foo` -> GitLab project `foo/bar`
- [ ] Migrate bugs: Bugzilla product `foo`, component `bar` -> GitLab project `foo/bar`

## Mailman
<!--
     No migration is required for Mailman lists, but it's helpful for us to
     keep track of which lists should be associated with which admins, etc.
     Maybe this is a good time to let us know if there is anything you would
     like to see changed with the lists.
-->

- [ ] Mailman list: `foo-devel@lists.fd.o`

## Web hosting
<!--
     We are recommending untangling projects from ikiwiki and the www.fd.o
     namespace, to allow you to be in full control of your site. Our hosting
     here is through GitLab Pages: create a repository with a GitLab CI pipeline
     which produces public/ as its artifacts.

     This is a little more work, but means you are less dependent on us. A
     GitLab CI pipeline can run static site generators such as Jekyll or
     Hugo in its own container - with the environment fully controlled by you -
     or just copy existing static HTML pages. It can also be forked by others:
     they can experiment with modifying the CI pipeline to see what your site
     would look like, so you can evaluate changes to your site just like code.

     We are not recommending the use of GitLab's built-in wiki. Whilst it is
     easy to get started with, it does not allow others to fork it and suggest
     changes, and does not allow you to host tarballs either. Wiki permission
     cannot really be given separately to code-pushing permission.

     Migrating your site to Pages need not be done immediately; you can
     experiment in your own sandbox as you have time, and migrate only later.
     Documentation and example links:
       https://docs.gitlab.com/ee/user/project/pages/introduction.html
       https://gitlab.freedesktop.org/wayland/wayland.freedesktop.org
       https://gitlab.freedesktop.org/libfprint/fprint.freedesktop.org
-->
- [ ] Separate wiki: `libbaz.fd.o` to migrate to Pages
- [ ] Wiki namespace: `www.fd.o/Software/quuxd` to migrate to Pages

/label ~GitLab ~Migration
/assign @daniels
